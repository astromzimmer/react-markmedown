import React from 'react'
import Markdown from 'react-markdown'
import {
	FaBold,
	FaItalic,
	FaQuoteRight,
	FaChain,
	FaBarcode
} from 'react-icons/lib/fa'

import styles from './styles.sss'

export default class MarkMeDown extends React.Component {
	constructor(props) {
	  super(props)
	  this.state = {
	  	value: props.value
	  }
	}
	componentWillReceiveProps(nextProps) {
		const { value } = nextProps
		if(this.state.value !== value)
			this.setState({ value })
	}
	onBoldClick(e) {
		this.wrapSelection('**')
	}
	onItalicClick(e) {
		this.wrapSelection('_')
	}
	onLinkClick(e) {
		const href = prompt("Please enter a URL\n( http://domain.com/something, or just /something )")
		if(href) this.wrapSelection('[',`](${href})`)
	}
	onRefClick(e) {
		const href = prompt("Please enter an object ID\n( ex. HSLU_000000123 )")
		if(href) this.wrapSelection('[',`](${href})`)
	}
	onQuoteClick(e) {
		const quotee = prompt("Who said that?")
		if(quotee) this.wrapSelection('> ',`/${quotee}`)
	}
	wrapSelection(prefix,postfix=prefix) {
		const { value } = this.state
		const { selectionStart, selectionEnd } = this.refs.textarea
		const before = value.substring(0,selectionStart)
		const selection = value.substring(selectionStart,selectionEnd)
		const after = value.substring(selectionEnd)
		const new_value = `${before}${prefix}${selection}${postfix}${after}`
		this.setState({
			value: new_value
		})
		this.triggerChange(new_value)
	}
	triggerChange(value) {
		const { onChange } = this.props
		onChange(value)
	}
	render() {
		const { onChange, renderers } = this.props
		const { value } = this.state
		return (
			<div className="markmedown">
				<div className="toolbar">
					<div className="tool bold"
						ref="bold"
						onClick={ this.onBoldClick.bind(this) }><FaBold/></div>
					<div className="tool italic"
						ref="italic"
						onClick={ this.onItalicClick.bind(this) }><FaItalic/></div>
					<div className="tool quote"
						ref="quote"
						onClick={ this.onQuoteClick.bind(this) }><FaQuoteRight/></div>
					<div className="tool link"
						ref="link"
						onClick={ this.onLinkClick.bind(this) }><FaChain/></div>
					<div className="tool ref"
						ref="ref"
						onClick={ this.onRefClick.bind(this) }><FaBarcode/></div>
				</div>
				<div
					className="flexer">
					<textarea
						ref="textarea"
						name="text"
						value={ value }
						onChange={ onChange } />
					<div
						className="md">
						<Markdown
							source={ value }
							renderers={ renderers } />
					</div>
				</div>
			</div>
		)
	}
}