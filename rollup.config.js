import babel from 'rollup-plugin-babel'
import json from 'rollup-plugin-json'
import commonjs from 'rollup-plugin-commonjs'
import postcss from 'rollup-plugin-postcss'
import resolve from 'rollup-plugin-node-resolve'

import sugarss from 'sugarss'

import pkg from './package.json'

export default {
	input: 'src/index.jsx',
	output: [
		{
			file: pkg.main,
			format: 'cjs'
		}
	],
	external: [
		'react',
		'react-dom',
		'prop-types',
		'react-icons',
		'react-markdown'
	],
	plugins: [
		resolve(),
		json(),
		commonjs(),
		postcss({
			parser: sugarss
		}),
		babel({
			exclude: 'node_modules/**'
		})
	]
}