'use strict';

function __$styleInject(css, returnValue) {
  if (typeof document === 'undefined') {
    return returnValue;
  }
  css = css || '';
  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';
  head.appendChild(style);
  
  if (style.styleSheet){
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
  return returnValue;
}

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var React = _interopDefault(require('react'));
var Markdown = _interopDefault(require('react-markdown'));
var fa = require('react-icons/lib/fa');

__$styleInject(".markmedown >\n.flexer {\n\tdisplay: flex;\n\tflex-direction: row;\n\theight: 56vh\n}\n\n.markmedown >\n.flexer >\ntextarea {\n\twidth: 50%;\n\theight: 100%\n}\n\n.markmedown >\n.flexer >\n.md {\n\twidth: 50%;\n\theight: 100%;\n\toverflow: auto\n}\n\n.markmedown >\n.toolbar {\n\tdisplay: flex\n}\n\n.markmedown >\n.toolbar >\n.tool {\n\tpadding: .2em .4em;\n\tcursor: pointer;\n\tborder-width: 2px;\n\tborder-style: solid;\n\tborder-color: transparent\n}\n\n.markmedown >\n.toolbar >\n.tool:hover {\n\tborder-color: black\n}", undefined);

var asyncGenerator = function () {
  function AwaitValue(value) {
    this.value = value;
  }

  function AsyncGenerator(gen) {
    var front, back;

    function send(key, arg) {
      return new Promise(function (resolve, reject) {
        var request = {
          key: key,
          arg: arg,
          resolve: resolve,
          reject: reject,
          next: null
        };

        if (back) {
          back = back.next = request;
        } else {
          front = back = request;
          resume(key, arg);
        }
      });
    }

    function resume(key, arg) {
      try {
        var result = gen[key](arg);
        var value = result.value;

        if (value instanceof AwaitValue) {
          Promise.resolve(value.value).then(function (arg) {
            resume("next", arg);
          }, function (arg) {
            resume("throw", arg);
          });
        } else {
          settle(result.done ? "return" : "normal", result.value);
        }
      } catch (err) {
        settle("throw", err);
      }
    }

    function settle(type, value) {
      switch (type) {
        case "return":
          front.resolve({
            value: value,
            done: true
          });
          break;

        case "throw":
          front.reject(value);
          break;

        default:
          front.resolve({
            value: value,
            done: false
          });
          break;
      }

      front = front.next;

      if (front) {
        resume(front.key, front.arg);
      } else {
        back = null;
      }
    }

    this._invoke = send;

    if (typeof gen.return !== "function") {
      this.return = undefined;
    }
  }

  if (typeof Symbol === "function" && Symbol.asyncIterator) {
    AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
      return this;
    };
  }

  AsyncGenerator.prototype.next = function (arg) {
    return this._invoke("next", arg);
  };

  AsyncGenerator.prototype.throw = function (arg) {
    return this._invoke("throw", arg);
  };

  AsyncGenerator.prototype.return = function (arg) {
    return this._invoke("return", arg);
  };

  return {
    wrap: function (fn) {
      return function () {
        return new AsyncGenerator(fn.apply(this, arguments));
      };
    },
    await: function (value) {
      return new AwaitValue(value);
    }
  };
}();





var classCallCheck = function (instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
};

var createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();









var inherits = function (subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function, not " + typeof superClass);
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      enumerable: false,
      writable: true,
      configurable: true
    }
  });
  if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass;
};











var possibleConstructorReturn = function (self, call) {
  if (!self) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return call && (typeof call === "object" || typeof call === "function") ? call : self;
};

var MarkMeDown = function (_React$Component) {
	inherits(MarkMeDown, _React$Component);

	function MarkMeDown(props) {
		classCallCheck(this, MarkMeDown);

		var _this = possibleConstructorReturn(this, (MarkMeDown.__proto__ || Object.getPrototypeOf(MarkMeDown)).call(this, props));

		_this.state = {
			value: props.value
		};
		return _this;
	}

	createClass(MarkMeDown, [{
		key: 'componentWillReceiveProps',
		value: function componentWillReceiveProps(nextProps) {
			var value = nextProps.value;

			if (this.state.value !== value) this.setState({ value: value });
		}
	}, {
		key: 'onBoldClick',
		value: function onBoldClick(e) {
			this.wrapSelection('**');
		}
	}, {
		key: 'onItalicClick',
		value: function onItalicClick(e) {
			this.wrapSelection('_');
		}
	}, {
		key: 'onLinkClick',
		value: function onLinkClick(e) {
			var href = prompt("Please enter a URL\n( http://domain.com/something, or just /something )");
			if (href) this.wrapSelection('[', '](' + href + ')');
		}
	}, {
		key: 'onRefClick',
		value: function onRefClick(e) {
			var href = prompt("Please enter an object ID\n( ex. HSLU_000000123 )");
			if (href) this.wrapSelection('[', '](' + href + ')');
		}
	}, {
		key: 'onQuoteClick',
		value: function onQuoteClick(e) {
			var quotee = prompt("Who said that?");
			if (quotee) this.wrapSelection('> ', '/' + quotee);
		}
	}, {
		key: 'wrapSelection',
		value: function wrapSelection(prefix) {
			var postfix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : prefix;
			var value = this.state.value;
			var _refs$textarea = this.refs.textarea,
			    selectionStart = _refs$textarea.selectionStart,
			    selectionEnd = _refs$textarea.selectionEnd;

			var before = value.substring(0, selectionStart);
			var selection = value.substring(selectionStart, selectionEnd);
			var after = value.substring(selectionEnd);
			var new_value = '' + before + prefix + selection + postfix + after;
			this.setState({
				value: new_value
			});
			this.triggerChange(new_value);
		}
	}, {
		key: 'triggerChange',
		value: function triggerChange(value) {
			var onChange = this.props.onChange;

			onChange(value);
		}
	}, {
		key: 'render',
		value: function render() {
			var _props = this.props,
			    onChange = _props.onChange,
			    renderers = _props.renderers;
			var value = this.state.value;

			return React.createElement(
				'div',
				{ className: 'markmedown' },
				React.createElement(
					'div',
					{ className: 'toolbar' },
					React.createElement(
						'div',
						{ className: 'tool bold',
							ref: 'bold',
							onClick: this.onBoldClick.bind(this) },
						React.createElement(fa.FaBold, null)
					),
					React.createElement(
						'div',
						{ className: 'tool italic',
							ref: 'italic',
							onClick: this.onItalicClick.bind(this) },
						React.createElement(fa.FaItalic, null)
					),
					React.createElement(
						'div',
						{ className: 'tool quote',
							ref: 'quote',
							onClick: this.onQuoteClick.bind(this) },
						React.createElement(fa.FaQuoteRight, null)
					),
					React.createElement(
						'div',
						{ className: 'tool link',
							ref: 'link',
							onClick: this.onLinkClick.bind(this) },
						React.createElement(fa.FaChain, null)
					),
					React.createElement(
						'div',
						{ className: 'tool ref',
							ref: 'ref',
							onClick: this.onRefClick.bind(this) },
						React.createElement(fa.FaBarcode, null)
					)
				),
				React.createElement(
					'div',
					{
						className: 'flexer' },
					React.createElement('textarea', {
						ref: 'textarea',
						name: 'text',
						value: value,
						onChange: onChange }),
					React.createElement(
						'div',
						{
							className: 'md' },
						React.createElement(Markdown, {
							source: value,
							renderers: renderers })
					)
				)
			);
		}
	}]);
	return MarkMeDown;
}(React.Component);

module.exports = MarkMeDown;
